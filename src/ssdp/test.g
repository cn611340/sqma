
in_nested_list := function(x,L)
    local match, i;
    match:=false;
    i := 1;
    while not(match) and i <= Length(L) do
        match := x in L[i];
        i := i + 1;
    od;
    return match;
end;;
         
effects_to_effects := function(A, Gen)
    local check;
    check:=true;
    for M in A do
        for E in M do
            for Ug in Gen do
                check := check and in_nested_list(Ug*E*Inverse(Ug),A);
            od;
        od;
    od; 
    return check;
end;;

z_to_xa := function(A)
    local pirec,labrec, zrec, z, x, a;
    z := 1;
    x := 1;
    a := 1;
    Elist := [];
    labrec := rec();
    for Meas in A do
        for Effect in M do
            Add(Elist,Effect);
            labrec.z := [a,x];
            pirec.z := x;
            z := z+1;
            a := a+1;
        od;
        x := x + 1;
        a := 1;
    od;
    return [labrec, pirec, Elist];
end;;

rec_length := function(r)
    local l;
    l := 0;
    for rr in r do
        l:=l+1;
    od;
    return l;
end;;

get_permutations := function(A,Gen)
    local ll, labrec, pirec, Elist;
    ll := z_to_xa(A);
    labrec := ll[1];
    pirec := ll[2];
    Elist := ll[3];
    checkz := [1..Length(Elist)];
    pcyc := x -> PermutationCycle(x, Elist, Elist[1], Conjugation);
    perms := List(Group(Gen),pcyc)
        
    
     
    
