E = n -> exp(2*im*pi/n)
sq = sqrt([96 0; 0 96])
d = 2
k = 4
n = 2
mes = zeros(ComplexF64, 2, 2, 2, 4)
mes[:, :, 1, 1] = sq*[-1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11 -1/6*E(12)^4+1/6*E(12)^7+1/6*E(12)^8-1/6*E(12)^11; 1/6*E(12)^4+1/6*E(12)^7-1/6*E(12)^8-1/6*E(12)^11 -1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11]*sq^-1
mes[:, :, 2, 1] = sq*[-1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11 1/6*E(12)^4-1/6*E(12)^7-1/6*E(12)^8+1/6*E(12)^11; -1/6*E(12)^4-1/6*E(12)^7+1/6*E(12)^8+1/6*E(12)^11 -1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11]*sq^-1
mes[:, :, 1, 2] = sq*[-1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11 1/6*E(12)^4+1/6*E(12)^7-1/6*E(12)^8-1/6*E(12)^11; -1/6*E(12)^4+1/6*E(12)^7+1/6*E(12)^8-1/6*E(12)^11 -1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11]*sq^-1
mes[:, :, 2, 2] = sq*[-1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11 -1/6*E(12)^4-1/6*E(12)^7+1/6*E(12)^8+1/6*E(12)^11; 1/6*E(12)^4-1/6*E(12)^7-1/6*E(12)^8+1/6*E(12)^11 -1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11]*sq^-1
mes[:, :, 1, 3] = sq*[-1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11 -1/6*E(12)^4+1/6*E(12)^7+1/6*E(12)^8-1/6*E(12)^11; 1/6*E(12)^4+1/6*E(12)^7-1/6*E(12)^8-1/6*E(12)^11 -1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11]*sq^-1
mes[:, :, 2, 3] = sq*[-1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11 1/6*E(12)^4-1/6*E(12)^7-1/6*E(12)^8+1/6*E(12)^11; -1/6*E(12)^4-1/6*E(12)^7+1/6*E(12)^8+1/6*E(12)^11 -1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11]*sq^-1
mes[:, :, 1, 4] = sq*[-1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11 1/6*E(12)^4+1/6*E(12)^7-1/6*E(12)^8-1/6*E(12)^11; -1/6*E(12)^4+1/6*E(12)^7+1/6*E(12)^8-1/6*E(12)^11 -1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11]*sq^-1
mes[:, :, 2, 4] = sq*[-1/2*E(12)^4-1/6*E(12)^7-1/2*E(12)^8+1/6*E(12)^11 -1/6*E(12)^4-1/6*E(12)^7+1/6*E(12)^8+1/6*E(12)^11; 1/6*E(12)^4-1/6*E(12)^7-1/6*E(12)^8+1/6*E(12)^11 -1/2*E(12)^4+1/6*E(12)^7-1/2*E(12)^8-1/6*E(12)^11]*sq^-1
